README.md
---------

## Overview
In the `.gitlab-ci.yml` file we reference a Docker Image to execute the commands. These steps were used (along with the `Dockerfile` in this directory).

### Build the image
The `Dockerfile` installs Ansible and Ansible Lint. Because we are using `ansible-galaxy` to install the ACI collection, we need  Ansible 2.9.x to properly install the collection and Lint the playbook(s).

```bash
docker build -f ./Dockerfile -t joelwking/ansible:1.1 .
```
Run the docker image locally to verify it has built and executes correctly.

```bash
docker run -it joelwking/ansible:1.1
```
The output from running the image should show the version of Ansible installed.

```bash
ansible-playbook 2.10.7
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/local/lib/python3.8/site-packages/ansible
  executable location = /usr/local/bin/ansible-playbook
  python version = 3.8.9 (default, Apr  2 2021, 21:57:08) [GCC 10.2.1 20201203]
```
View the charastics of the image

```bash
administrator@olive-iron:~/docker/cicd$ docker images joelwking/ansible:1.1
REPOSITORY          TAG       IMAGE ID       CREATED          SIZE
joelwking/ansible   1.1       1049b5ab8580   13 minutes ago   656MB

```
### Publish the image
In order to reference this Docker image in the `.gitlab-ci.yml` pipeline file, we need to publish the image on Docker Hub. First login from the machine which build the image.
```bash
docker login --username=joelwking
```
#### Create the repository on DockerHub
Log on your DockerHub account and create the repo name. For example, https://hub.docker.com/repository/docker/joelwking/ansible/general.

>**Note**: in this example, we specified the repository name `joelwking/ansible` and tag `1.1` when we built the image. However, it can be done separately as shown.
```bash
docker tag f1ce7db4e4eb joelwking/ansible:1.1
```
Now push the image to Docker Hub.
```bash
docker push joelwking/ansible:1.1
```
We can now reference this image in the `.gitlab-ci.yml` file.

## References
https://ropenscilabs.github.io/r-docker-tutorial/04-Dockerhub.html

## Author
Joel W. King  @joelwking